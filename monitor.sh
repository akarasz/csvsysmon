#!/bin/bash

timestamp=$(date +"%Y.%m.%d %H:%M:%S")
load=$(uptime | tr -s ' ' | cut -d, -f4- | cut -d: -f2 | tr -d ' ')
memory=$(free | head -n 3 | tail -n 1 | tr -s ' ' | cut -d' ' -f 3)
swap=$(free | head -n 4 | tail -n 1 | tr -s ' ' | cut -d' ' -f 3)

echo "$timestamp,$load,$memory,$swap"
